function ohMyBlocks() {
  // we'll just return some stuff...
  return
  {
    response: 'omg'
  };
}


// Gotta be careful with switch statements
function getWeekDay(dayNum) {
  var day = null;
  switch (dayNum) {
    case 0:
      day = "Monday";
      break;
    case 1:
      day = "Tuesday";
      break;
    case 2:
      day = "Wednesday";
      break;
    case 3:
      day = "Thursday";
      break;
    case 4:
      day = "Friday";
      break;
    case 5:
      day = "Saturday";
    default:
      day = "Sunday";
  }
  return day;
}


// This one is a mess...
function isSquare(a, b, a) {
  var square = b * b;;
  if (a = square) {
    return true
  }
  else {
    return false;
  }
}


// No constants in test conditions
function generateTen() {
  var i = 0;
  var numbers = []
  for (;true;) {
    numbers.push(i++);
    if (i>=10)
      break;
  }
  return numbers;
}



// How many items are in this array?
var items = [,,,];


// Does 0 equals '0'? How about false?
function equalsStringZero(foo) {
  return foo == '0';
}


// NaN is unique in JavaScript by not being equal to anything, including itself
function checkNaN(value) {
  if (value == NaN)
    return true;
  else if (value === NaN)
    return true;
}


// ESlint rule valid-typeof requires typeof expressions to only be compared
// to string literals or other typeof expressions
var foo = "asdf";
typeof foo === "strnig";
typeof foo == "undefimed";
typeof bar != "nunber";
typeof bar !== "fucntion";


// Just some stylistic issues - mostly subjective
getWeekDay (0);

function isEven( i ){return i % 2 === 0;}

function is_odd( i ){return i % 2 !== 0;}  // consider tests...

function commaSpacing() {
  var one = 1 ,two = 2;
  var arr = [1 , 2];
  return {"one": one ,"two": two , "arr": arr};
}

function keySpacing() {
  return { "foo" : 42, "bar":10 };
}

function fibonacci(num){
    var a = 1,b = 0,temp;
    while (num >= 0)
    {
        temp = a;
        a = a + b;
        b = temp;
        num--;
    }
    return b;
}

function fibonacciRecursive(num) {
  if (num <= 1) return 1;
  return fibonacciRecursive(num -  1)  + fibonacciRecursive(num  - 2);
}

function mean(array) {
  //  Mean (arithmetic average) of a numeric vector
  var sum = 0, i;
  for (i = 0;  i < array.length; i++) {
    sum += array[i];
  }
  return array.length ? sum / array.length : 0;
}

function meanEach(array) {
  //  Mean (arithmetic average) of a numeric vector done with foreach
  var sum=0;
  array.forEach(function(value){
      sum += value;
  });
  return array.length ? sum / array.length : 0;
}

function median(ary) {
  // The median is the value separating the higher half of a data sample,
  // a population, or a probability distribution, from the lower half.
  // In simple terms, it may be thought of as the "middle" value of a data set.
    if (0 == ary.length)
      return null;
    ary.sort(function (a,b){return a - b})
    var mid = Math.floor(ary.length / 2);
    if ((ary.length % 2) == 1)  // length is odd
      return ary[mid];
    else
      return (ary[mid - 1] + ary[mid]) / 2;
}

function mode(ary) {
  // The mode is the value that appears most often in a set of data.
  counter = {};
  mode = [];
  max = 0;
  for (var i in ary) {
    if (!(ary[i] in counter))
      counter[ary[i]] = 0;
    counter[ary[i]]++;

    if (counter[ary[i]] == max)
      mode.push(ary[i]);
    else if (counter[ary[i]] > max) {
      max = counter[ary[i]];
      mode = [ary[i]];
    }
  }
  return mode;
}




function factorial(n) {
  // Return the factorial of a number
  if (0 > n) { throw "Number must be non-negative"; }

  var sum = 1;
  //we skip zero and one since both are 1 and are identity
  while (n > 1) {
    sum *= n;
    n -= 1;
  }
  return sum;
}


QUnit.test( "Gotchas", function( assert ) {
  assert.deepEqual( ohMyBlocks(), {response: 'omg'});

  assert.equal( getWeekDay(0), "Monday");
  assert.equal( getWeekDay(1), "Tuesday");
  assert.equal( getWeekDay(4), "Friday");
  assert.equal( getWeekDay(5), "Saturday");
  assert.equal( getWeekDay(), "Sunday");

  assert.equal( isSquare(9, 3), true);
  assert.equal( isSquare(4, 2), true);
  assert.equal( isSquare(3, 1), false);
  assert.equal( isSquare(2, 1), false);

  assert.equal( generateTen(), [1, 2, 3, 4, 5, 6, 7, 8, 9]);

  assert.equal( items.length, 0);

  assert.equal( equalsStringZero(0), false);
  assert.equal( equalsStringZero(false), false);

  assert.equal( checkNaN(NaN), true);

  assert.equal( isEven(1), false );
  assert.equal( isEven(2), true );
  assert.equal( isEven(3), false );
  assert.equal( isEven(4), true );

  assert.equal( is_odd(1), true );
  assert.equal( is_odd(2), false );
  assert.equal( is_odd(3), true );
  assert.equal( is_odd(4), false );

  assert.deepEqual(commaSpacing(), {"one": 1, "two": 2, "arr": [1, 2]});
  assert.deepEqual(keySpacing(), { "foo": 42, "bar": 10 });
});

QUnit.test( "Rosetta", function( assert ) {
  assert.ok( fibonacci(1) == 1 );
  assert.ok( fibonacci(2) == 2 );
  assert.ok( fibonacci(3) == 3 );
  assert.ok( fibonacci(4) == 5 );
  assert.ok( fibonacci(7) == 21 );
  assert.ok( fibonacci(10) == 89 );

  assert.ok( fibonacciRecursive(1) == 1 );
  assert.ok( fibonacciRecursive(2) == 2 );
  assert.ok( fibonacciRecursive(3) == 3 );
  assert.ok( fibonacciRecursive(4) == 5 );
  assert.ok( fibonacciRecursive(7) == 21 );
  assert.ok( fibonacciRecursive(10) == 89 );

  assert.ok( mean([1, 2, 3, 4, 5]) == 3 );
  assert.ok( mean([]) == 0 );

  assert.ok( meanEach([1, 2, 3, 4, 5]) == 3 );
  assert.ok( meanEach([]) == 0 );

  assert.ok( median([]) == null );
  assert.ok( median([5, 3, 4]) == 4 );
  assert.ok( median([5, 4, 2, 3]) == 3.5 );
  assert.ok( median([3, 4, 1, -8.4, 7.2, 4, 1, 1.2]) == 2.1 );

  assert.deepEqual( mode([1, 3, 6, 6, 6, 6, 7, 7, 12, 12, 17]), [6] );
  assert.deepEqual( mode([1, 2, 4, 4, 1]), [4, 1] );

  assert.throws( function(){
    factorial(-1);
  });
  assert.equal( factorial(1), 1 );
  assert.equal( factorial(3), 6 );
  assert.equal( factorial(6), 720 );

});
